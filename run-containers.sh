cd docker

docker-compose up -d

docker-compose exec php24i composer install

docker-compose exec php24i php artisan migrate

docker-composer exec php24i php artisan passport:install

docker-composer exec php24i php artisan config:cache

docker-composer exec php24i php artisan route:cache

docker-composer exec php24i php artisan optimize --force

docker-composer exec php24i composer dumpautoload -o

