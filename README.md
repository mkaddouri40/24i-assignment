<h1> Intoduction </h1>
<br>
This is a project/assignment that was created for 24I.
In this readme file. I will explain why, I choose the way I programmed the api.
And I will explain short how the api works and the techniques I Used.

<h4> Docker environment pre-stup </h4>
Before running our containers. We must Create a database.
<ul>
<li> named: 24idb </li>
</ul>


<h4> Docker environment pre-stup </h4>
Now that the database is created, you can run
the shell scripts i provided in the following order.

<ol>
<li> buil.sh </li>
<li> run-containers.sh </li>
</ol>

<h4> Interacting with the api </h4>
To use and test the api you can serve to: <a href="http://localhost:9090/api/documentation">Api Documentation</a>
<br>
There are routes that will need to have a bearer token. To get hold of a bearer token you must first register a user. <br>
Under the section Auth and then the action register. Once you are registered, you need to login with the account you just created.
Copy your token. <br>
By clicking on the key lock icon (any one of them will suffice) Paste your token like so: i.e. Bearer mytoken
<br>
<h4> Choices explained </h4>
When it comes to the entertainment industry I tend to think about Movies and music.
<br>
I created two tables and one intermediating 2 table.
<br>
2 models and one pivot table.
<br>
the relationship is a many to to many relationship.
<br>
One Movie has many Genres
A Genre can belong to many Movies.

<h1>Patterns</h1>
I chose to use the repository pattern. So that I can seperate the data access layers from the business logic 
<br>
I also used the service layer pattern to provide logic to operate on the data sent to and from the api.

<h1>Authentication</h1>
For authentication I used Laravel passport because it's a great package that provides good security and it's easy and fast to implement,
<br>
eg the life time of tokens.

<h1>HTTP Status code</h1>
Through the use of exceptions I used the correct http status code for different scenario's, eg Movie not found, which returns a 404.

<h1>Code standards</h1>
I use psr5 code standards to format my code.


<h1>MicroService</h1>
I did not build it as a microservice. 
<br>
I understand the logic and theory behind microservice, but I never had to use them. But that is something I would love to learn.

I Hope given enough information for now, please feel free to contact me at any giving time: 
by mail or phone
m.kaddouri@live.com 
0614674568



