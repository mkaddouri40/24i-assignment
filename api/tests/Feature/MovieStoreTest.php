<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 24/07/2020
 * Time: 17:33
 */

namespace Tests\Feature;


use App\Models\Genre;
use Tests\TestCase;

class MovieStoreTest extends TestCase
{

    public function testStore()
    {
        $genre = factory(Genre::class,20)->create();

        $response = $this->post('/api/movies/store', [
            'name' => 'test movie: ' . rand(1, 1000),
            'genres' => $genre->pluck('id')->toArray()
        ]);

        $response->dump();

        $response->assertJsonStructure([
            'id',
            'name',
            'created_at' => [
                'date',
                'timezone_type',
                'timezone'
            ],
            'updated_at' => [
                'date',
                'timezone_type',
                'timezone'
            ]
        ]);
    }
}
