<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MovieUpdateTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/api/movies/update', [
            'id' => '',
            'name' => 'test movie update: ' . rand(1, 1000)
        ]);

        $response->assertStatus(200);
    }
}
