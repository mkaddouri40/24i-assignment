<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MovieSearchTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSearchMovie()
    {
        $response = $this->post('/api/movies', [
            'limit' => 1,
            'page' => 3,
            'order_by' => 'id',
            'direction' => 'asc'
        ]);

        $response->assertStatus(200);
    }
}
