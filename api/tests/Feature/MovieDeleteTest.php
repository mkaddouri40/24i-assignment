<?php

namespace Tests\Feature;

use App\Models\Movie;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MovieDeleteTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testDeleteMovie()
    {
        $movie = factory(Movie::class)->create();
        $response = $this->get('api/movies/delete/' . $movie->id);
//
        $response->assertStatus(202);
    }
}
