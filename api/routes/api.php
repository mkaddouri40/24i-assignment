<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('movies')->group(function () {
    Route::post('/', 'Movie\MovieController@index');
    Route::get('/{id}', 'Movie\MovieController@findById');
    Route::post('/store', 'Movie\MovieController@store')->middleware('auth:api');
    Route::post('/store/collection', 'Movie\MovieController@storeCollection')->middleware('auth:api');
    Route::post('/update', 'Movie\MovieController@update')->middleware('auth:api');
    Route::get('/delete/{id}', 'Movie\MovieController@delete')->middleware('auth:api');
});

Route::prefix('genres')->group(function () {
    Route::post('/', 'Genre\GenreController@index');
    Route::get('/{id}', 'Genre\GenreController@findById');
    Route::post('/store', 'Genre\GenreController@store')->middleware('auth:api');
    Route::post('/update', 'Genre\GenreController@update')->middleware('auth:api');
    Route::get('/delete/{id}', 'Genre\GenreController@delete')->middleware('auth:api');
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', 'Auth\AuthController@login');
    Route::post('/signup', 'Auth\AuthController@signup');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('/logout', 'AuthController@logout');
        Route::get('/user', 'AuthController@user')->middleware('auth:api');
    });
});

//a090ee40-e7d2-499e-9f92-f3443ed6f5d3
