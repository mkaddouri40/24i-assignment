<?php

namespace App\Models;

use App\Models\Concerns\UsesUuid;
use App\Src\Models\Search\SearchModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movie extends Model
{
    use UsesUuid;

    use SoftDeletes;

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;


    public function genres()
    {
        return $this->belongsToMany(
            Genre::class,
            'genre_movie',
            'movie_id',
            'genre_id',
            'id',
            'id'
        );
    }

    public function scopeId($query, $id)
    {
        return $id ? $query->where('id', 'LIKE', '%' . $id . '%') : null;
    }

    public function scopeName($query, $name)
    {
        return $name ? $query->where('name', 'LIKE', '%' . $name . '%') : null;
    }

    public function scopeSearch($query, SearchModel $searchModel)
    {
        $movie = $searchModel->getMovie();

        $query->Id(methodExistOrNull($movie, 'getId'))
            ->Name(methodExistOrNull($movie, 'getName'));

        return $query->get();
    }
}
