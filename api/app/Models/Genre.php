<?php

namespace App\Models;

use App\Models\Concerns\UsesUuid;
use App\Src\Models\Search\SearchModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Genre extends Model
{
    use UsesUuid;

    use SoftDeletes;

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    public function movies()
    {
        return $this->belongsToMany(
            Movie::class,
            'genre_movie',
            'genre_id',
            'movie_id',
            'id',
            'id'
        );
    }

    public function music()
    {
        return $this->belongsToMany(Music::class);
    }

    public function scopeId($query, $id)
    {
        return $id ? $query->where('id', 'LIKE', '%' . $id . '%') : null;
    }

    public function scopeName($query, $name)
    {
        return $name ? $query->where('name', 'LIKE', '%' . $name . '%') : null;
    }

    public function scopeSearch($query, SearchModel $searchModel)
    {
        $genre = $searchModel->getGenre();

        $query->Id(methodExistOrNull($genre, 'getId'))
            ->Name(methodExistOrNull($genre, 'getName'));

        return $query->get();
    }
}
