<?php

namespace App\Exceptions\Genre;

use Exception;
use Throwable;

class NoGenresAvailableException extends Exception
{
    public function __construct()
    {
        parent::__construct("No genres available at the moment! Check back the later", 204);
    }
}
