<?php

namespace App\Exceptions\Genre;

use Exception;
use Throwable;

class GenreNotFoundException extends Exception
{
    public function __construct()
    {
        parent::__construct("Genre not found", 404);
    }
}
