<?php

namespace App\Exceptions\User;

use Exception;

class EmailAllReadyExistsException extends Exception
{
    /**
     * EmailAllReadyExistsException constructor.
     */
    public function __construct()
    {

        parent::__construct("Email already exists", 400);
    }
}
