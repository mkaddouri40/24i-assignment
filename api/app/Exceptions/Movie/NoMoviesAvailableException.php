<?php

namespace App\Exceptions\Movie;

use Exception;
use Throwable;

class NoMoviesAvailableException extends Exception
{
    public function __construct()
    {
        parent::__construct("No movies available at the moment! Check back later", 204);
    }
}
