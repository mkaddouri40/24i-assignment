<?php

namespace App\Exceptions\Movie;

use Exception;
use Throwable;

class MovieNotFoundException extends Exception
{
    public function __construct()
    {
        parent::__construct("Movie not found", 404);
    }
}
