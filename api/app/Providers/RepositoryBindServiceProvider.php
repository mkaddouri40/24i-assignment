<?php

namespace App\Providers;

use App\Src\Repositories\Genre\GenreRepository;
use App\Src\Repositories\Genre\IGenreRepository;
use App\Src\Repositories\GenreMovie\GenreMovieRepository;
use App\Src\Repositories\GenreMovie\IGenreMovieRepository;
use App\Src\Repositories\Movie\IMovieRepository;
use App\Src\Repositories\Movie\MovieRepository;
use App\Src\Repositories\User\IUserRepository;
use App\Src\Repositories\User\UserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryBindServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            IMovieRepository::class,
            MovieRepository::class
        );

        $this->app->bind(
            IGenreRepository::class,
            GenreRepository::class
        );

        $this->app->bind(
            IGenreMovieRepository::class,
            GenreMovieRepository::class
        );

        $this->app->bind(
            IUserRepository::class,
            UserRepository::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
