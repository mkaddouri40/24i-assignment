<?php

namespace App\Providers;

use App\Src\Services\Auth\AuthService;
use App\Src\Services\Auth\IAuthService;
use App\Src\Services\Genre\GenreDeleteService;
use App\Src\Services\Genre\GenreService;
use App\Src\Services\Genre\GenreStoreService;
use App\Src\Services\Genre\GenreUpdateService;
use App\Src\Services\Genre\IGenreDeleteService;
use App\Src\Services\Genre\IGenreService;
use App\Src\Services\Genre\IGenreStoreService;
use App\Src\Services\Genre\IGenreUpdateService;
use App\Src\Services\GenreMovie\GenreMovieStoreService;
use App\Src\Services\GenreMovie\GenreMovieUpdateService;
use App\Src\Services\GenreMovie\IGenreMovieStoreService;
use App\Src\Services\GenreMovie\IGenreMovieUpdateService;
use App\Src\Services\Movie\IMovieDeleteService;
use App\Src\Services\Movie\IMovieService;
use App\Src\Services\Movie\IMovieStoreService;
use App\Src\Services\Movie\IMovieUpdateService;
use App\Src\Services\Movie\MovieDeleteService;
use App\Src\Services\Movie\MovieService;
use App\Src\Services\Movie\MovieStoreService;
use App\Src\Services\Movie\MovieUpdateService;
use App\Src\Services\User\IUserService;
use App\Src\Services\User\IUserStoreService;
use App\Src\Services\User\UserService;
use App\Src\Services\User\UserStoreService;
use Illuminate\Support\ServiceProvider;

class ServiceBindServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            IMovieStoreService::class,
            MovieStoreService::class
        );

        $this->app->bind(
            IMovieService::class,
            MovieService::class
        );

        $this->app->bind(
            IMovieDeleteService::class,
            MovieDeleteService::class
        );

        $this->app->bind(
            IMovieUpdateService::class,
            MovieUpdateService::class
        );

        $this->app->bind(
            IGenreStoreService::class,
            GenreStoreService::class
        );

        $this->app->bind(
            IGenreUpdateService::class,
            GenreUpdateService::class
        );

        $this->app->bind(
            IGenreService::class,
            GenreService::class
        );

        $this->app->bind(
            IGenreDeleteService::class,
            GenreDeleteService::class
        );

        $this->app->bind(
            IGenreMovieStoreService::class,
            GenreMovieStoreService::class
        );

        $this->app->bind(
            IGenreMovieUpdateService::class,
            GenreMovieUpdateService::class
        );

        $this->app->bind(
            IUserStoreService::class,
            UserStoreService::class
        );

        $this->app->bind(
            IUserService::class,
            UserService::class
        );

        $this->app->bind(
            IAuthService::class,
            AuthService::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
