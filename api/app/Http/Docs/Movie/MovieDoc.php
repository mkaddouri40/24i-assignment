<?php
/**
 * Movie doc
 * @OA\POST(
 *     path="/api/movies",
 *     tags={"Movies"},
 *     summary="Search for movies OR Get all ",
 * @OA\RequestBody(
 * @OA\MediaType(
 *             mediaType="application/json",
 * @OA\Schema(
 * @OA\Property(
 *                     property="page",
 *                     type="int"
 *                 ),
 * @OA\Property(
 *                     property="limit",
 *                     type="int"
 *                 ),
 * @OA\Property(
 *                     property="order_by",
 *                     type="string"
 *                 ),
 * @OA\Property(
 *                     property="direction",
 *                     type="string"
 *                 ),
 * @OA\Property(
 *                     property="search",
 *                     type="array",
 * @OA\Items(
 * @OA\Property(
 *                     property="id",
 *                     type="string"
 *                 ),
 * @OA\Property(
 *                     property="name",
 *                     type="string"
 *                 ),
 *                     ),
 *                 ),
 *                 example={"page": "1", "limit": "3", "order_by": "id",
 *                          "direction": "asc",
 *                "search": {"id": "uuid", "name": "Batman begins"},
 *                  }
 *             )
 *         )
 *     ),
 * @OA\Response(
 *         response="200",
 *         description="Successful operation!"
 *     ),
 * @OA\Response(
 *         response="204",
 *         description="No Movies available"
 *     ),
 * @OA\Response(
 *          response=500,
 *          description="Something went wrong!"
 *      ),
 * )
 */

/**
 * @OA\GET(
 *     path="/api/movies/{id}",
 *     tags={"Movies"},
 *     summary="Find a Movie",
 * @OA\Parameter(
 *          name="id",
 *          in="path"
 *     ),
 * @OA\Response(
 *         response="200",
 *         description="Successful operation!"
 *     ),
 * @OA\Response(
 *         response="404",
 *         description="Movie not found!"
 *     ),
 * @OA\Response(
 *         response="500",
 *         description="Server error!"
 *     ),
 * )
 */

/**
 * @OA\POST(
 *     path="/api/movies/store",
 *     tags={"Movies"},
 *     summary="Store a movie",
 *     security={{"passport": {}}},
 * @OA\RequestBody(
 * @OA\MediaType(
 *             mediaType="application/json",
 * @OA\Schema(
 * @OA\Property(
 *                     property="name",
 *                     type="string"
 *                 ),
 *                 @OA\Property(
 *                     property="genres",
 *                     type="array",
 *                     @OA\Items(
 *                     ),
 *                 ),
 *                 example={"name": "name of movie","genres": {1} }
 *             )
 *         )
 *     ),
 * @OA\Response(
 *         response="200",
 *         description="Successful operation!"
 *     ),
 * @OA\Response(
 *         response="500",
 *         description="Server error"
 *     ),
 * )
 */

/**
 * @OA\POST(
 *     path="/api/movies/store/collection",
 *     tags={"Movies"},
 *     summary="Store a collection of movies",
 *     security={{"passport": {}}},
 * @OA\RequestBody(
 * @OA\MediaType(
 *             mediaType="application/json",
 * @OA\Schema(
 *                 @OA\Property(
 *                     property="movie",
 *                     type="array",
 *                      @OA\Items(
 *                          @OA\Property(
 *                              property="name",
 *                               type="string"
 *                         ),
 *                       @OA\Property(
 *                         property="genres",
 *                         type="array",
 *                         @OA\Items(
 *                         ),
 *                 ),
 *                     ),
 *                 ),
 *                 example={ {"name": "name of movie","genres": {1}}}
 *             )
 *         )
 *     ),
 * @OA\Response(
 *         response="200",
 *         description="Successful operation!"
 *     ),
 * @OA\Response(
 *         response="500",
 *         description="Server error"
 *     ),
 * )
 */

/**
 * @OA\POST(
 *     path="/api/movies/update",
 *     tags={"Movies"},
 *     summary="Update a movie",
 *     security={{"passport": {}}},
 * @OA\RequestBody(
 * @OA\MediaType(
 *             mediaType="application/json",
 * @OA\Schema(
 * @OA\Property(
 *                     property="id",
 *                     type="string"
 *                 ),
 * @OA\Property(
 *                     property="name",
 *                     type="string"
 *                 ),
 *                 @OA\Property(
 *                     property="genres_remove",
 *                     type="array",
 *                     @OA\Items(
 *                     ),
 *                 ),
 *                 @OA\Property(
 *                     property="genres_add",
 *                     type="array",
 *                     @OA\Items(
 *                     ),
 *                 ),
 *                 example={"id": "id is an uuid", "name": "name of movie", "genres_remove": {1}, "genres_add": {1} }
 *             )
 *         )
 *     ),
 * @OA\Response(
 *         response="200",
 *         description="Successful operation!"
 *     ),
 * @OA\Response(
 *         response="404",
 *         description="Movie not found"
 *     ),
 * @OA\Response(
 *         response="500",
 *         description="Server error"
 *     ),
 * )
 */

/**
 * @OA\GET(
 *     path="/api/movies/delete/{id}",
 *     tags={"Movies"},
 *     summary="Delete a Movie",
 *     security={{"passport": {}}},
 * @OA\Parameter(
 *          name="id",
 *          in="path"
 *     ),
 * @OA\Response(
 *         response="202",
 *         description="Successful operation!"
 *     ),
 * @OA\Response(
 *         response="404",
 *         description="Movie not found!"
 *     ),
 * @OA\Response(
 *         response="500",
 *         description="Server error!"
 *     ),
 * )
 */
