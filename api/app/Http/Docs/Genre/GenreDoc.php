<?php
/**
 * Genre doc
 * @OA\POST(
 *     path="/api/genres",
 *     tags={"Genres"},
 *     summary="Search for genres OR Get all ",
 * @OA\RequestBody(
 * @OA\MediaType(
 *             mediaType="application/json",
 * @OA\Schema(
 * @OA\Property(
 *                     property="page",
 *                     type="int"
 *                 ),
 * @OA\Property(
 *                     property="limit",
 *                     type="int"
 *                 ),
 * @OA\Property(
 *                     property="order_by",
 *                     type="string"
 *                 ),
 * @OA\Property(
 *                     property="direction",
 *                     type="string"
 *                 ),
 * @OA\Property(
 *                     property="search",
 *                     type="array",
 * @OA\Items(
 * @OA\Property(
 *                     property="id",
 *                     type="string"
 *                 ),
 * @OA\Property(
 *                     property="name",
 *                     type="string"
 *                 ),
 *                     ),
 *                 ),
 *                 example={"page": "1", "limit": "3", "order_by": "id",
 *                          "direction": "asc",
 *                "search": {"id": "uuid", "name": "Batman begins"},
 *                  }
 *             )
 *         )
 *     ),
 * @OA\Response(
 *         response="200",
 *         description="Successful operation!"
 *     ),
 * @OA\Response(
 *         response="204",
 *         description="No Genres available"
 *     ),
 * @OA\Response(
 *          response=500,
 *          description="Something went wrong!"
 *      ),
 * )
 */

/**
 * @OA\GET(
 *     path="/api/genres/{id}",
 *     tags={"Genres"},
 *     summary="Find a Genre",
 * @OA\Parameter(
 *          name="id",
 *          in="path"
 *     ),
 * @OA\Response(
 *         response="200",
 *         description="Successful operation!"
 *     ),
 * @OA\Response(
 *         response="404",
 *         description="Genre not found!"
 *     ),
 * @OA\Response(
 *         response="500",
 *         description="Server error!"
 *     ),
 * )
 */

/**
 * @OA\POST(
 *     path="/api/genres/store",
 *     tags={"Genres"},
 *     summary="Store a genre",
 *     security={{"passport": {}}},
 * @OA\RequestBody(
 * @OA\MediaType(
 *             mediaType="application/json",
 * @OA\Schema(
 * @OA\Property(
 *                     property="name",
 *                     type="string"
 *                 ),
 *                 example={"name": "name of genre",}
 *             )
 *         )
 *     ),
 * @OA\Response(
 *         response="200",
 *         description="Successful operation!"
 *     ),
 * @OA\Response(
 *         response="500",
 *         description="Server error"
 *     ),
 * )
 */

/**
 * @OA\POST(
 *     path="/api/genres/update",
 *     tags={"Genres"},
 *     summary="Update a genre",
 *     security={{"passport": {}}},
 * @OA\RequestBody(
 * @OA\MediaType(
 *             mediaType="application/json",
 * @OA\Schema(
 * @OA\Property(
 *                     property="id",
 *                     type="string"
 *                 ),
 * @OA\Property(
 *                     property="name",
 *                     type="string"
 *                 ),
 *                 example={"id": "id must be a string", "name": "name of genre",}
 *             )
 *         )
 *     ),
 * @OA\Response(
 *         response="200",
 *         description="Successful operation!"
 *     ),
 * @OA\Response(
 *         response="404",
 *         description="Genre not found!"
 *     ),
 * @OA\Response(
 *         response="500",
 *         description="Server error"
 *     ),
 * )
 */

/**
 * @OA\GET(
 *     path="/api/genres/delete/{id}",
 *     tags={"Genres"},
 *     summary="Delete a Genre",
 *     security={{"passport": {}}},
 * @OA\Parameter(
 *          name="id",
 *          in="path"
 *     ),
 * @OA\Response(
 *         response="202",
 *         description="Successful operation!"
 *     ),
 * @OA\Response(
 *         response="404",
 *         description="Genre not found!"
 *     ),
 * @OA\Response(
 *         response="500",
 *         description="Server error!"
 *     ),
 * )
 */
