<?php

namespace App\Http\Requests\Genre;

use App\Http\Requests\Search\BaseRequest;
use App\Src\Mappers\Request\Genre\GenreRequestSearchMapper;
use Illuminate\Foundation\Http\FormRequest;

class GenreSearchRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'search.id' => 'string',
            'search.name' => 'string'
        ];

        return array_merge(parent::rules(), $rules);
    }

    /**
     * @return mixed
     */
    public function map()
    {
        return GenreRequestSearchMapper::toModel($this);
    }
}
