<?php

namespace App\Http\Requests\Genre;

use App\Src\Mappers\Request\Genre\GenreRequestStoreMapper;
use Illuminate\Foundation\Http\FormRequest;

class GenreStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string'
        ];
    }

    /**
     * @return \App\Src\Models\Genre\GenreModel
     */
    public function map()
    {
        return GenreRequestStoreMapper::toModel($this);
    }
}
