<?php

namespace App\Http\Requests\User;

use App\Src\Mappers\Request\User\UserStoreRequestMapper;
use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email',
            'password' => 'required|string|min:12|max:30',
        ];
    }

    /**
     * @return \App\Src\Models\User\UserModel
     */
    public function map()
    {
        return UserStoreRequestMapper::toModel($this);
    }
}
