<?php

namespace App\Http\Requests\User;

use App\Src\Mappers\Request\User\UserAuthRequestMapper;
use Illuminate\Foundation\Http\FormRequest;

class UserAuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ];
    }

    /**
     * @return \App\Src\Models\Auth\AuthModel
     */
    public function map()
    {
        return UserAuthRequestMapper::toAuthModel($this);
    }
}
