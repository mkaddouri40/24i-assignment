<?php

namespace App\Http\Requests\Movie;

use App\Http\Requests\Search\BaseRequest;
use App\Src\Mappers\Request\Movie\MovieRequestSearchMapper;
use Illuminate\Foundation\Http\FormRequest;

class MovieSearchRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'search.id' => 'string',
            'search.name' => 'string'
        ];

        return array_merge(parent::rules(), $rules);
    }

    /**
     * @return mixed
     */
    public function map()
    {
        return MovieRequestSearchMapper::toModel($this);
    }
}
