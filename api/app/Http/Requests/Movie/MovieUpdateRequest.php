<?php

namespace App\Http\Requests\Movie;

use App\Src\Mappers\Request\Movie\MovieRequestUpdateMapper;
use Illuminate\Foundation\Http\FormRequest;

class MovieUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|string',
            'name' => 'required|string',
            'genres_remove' => 'array',
            'genres_add' => 'required|array'
        ];
    }

    /**
     * @return \App\Src\Models\Movie\MovieModel
     */
    public function map()
    {
        return MovieRequestUpdateMapper::toModel($this);
    }
}
