<?php

namespace App\Http\Requests\Movie;

use App\Src\Mappers\Request\Movie\MovieRequestStoreCollectionMapper;
use App\Src\Mappers\Request\Movie\MovieRequestStoreMapper;
use Illuminate\Foundation\Http\FormRequest;

class MovieStoreCollectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "movies" => "required|array",
            "movies.*.name" => "required",
            "movies.*.genres" => "required|array",
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function map()
    {
        return MovieRequestStoreCollectionMapper::toMovieModel($this);
    }
}
