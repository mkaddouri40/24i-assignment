<?php

namespace App\Http\Requests\Movie;

use App\Src\Mappers\Request\Movie\MovieRequestStoreMapper;
use Illuminate\Foundation\Http\FormRequest;

class MovieStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            "genres" => 'required|array'
        ];
    }

    /**
     * @return \App\Src\Models\Movie\MovieModel
     */
    public function map()
    {
        return MovieRequestStoreMapper::toMovieModel($this);
    }
}
