<?php

namespace App\Http\Controllers\Movie;

use App\Exceptions\Movie\MovieNotFoundException;
use App\Exceptions\Movie\NoMoviesAvailableException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Movie\MovieSearchRequest;
use App\Http\Requests\Movie\MovieStoreCollectionRequest;
use App\Http\Requests\Movie\MovieStoreRequest;
use App\Http\Requests\Movie\MovieUpdateRequest;
use App\Src\Mappers\Movie\MovieModelMapper;
use App\Src\Mappers\Search\Movie\SearchMovieModelMapper;
use App\Src\Mappers\Search\SearchModelMapper;
use App\Src\Responses\JsonResponse;
use App\Src\Services\Movie\IMovieDeleteService;
use App\Src\Services\Movie\IMovieService;
use App\Src\Services\Movie\IMovieStoreService;
use App\Src\Services\Movie\IMovieUpdateService;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    /**
     * @var IMovieStoreService
     */
    private $movieStoreService;

    /**
     * @var IMovieService
     */
    private $movieService;

    /**
     * @var IMovieDeleteService
     */
    private $movieDeleteService;

    /**
     * @var IMovieUpdateService
     */
    private $movieUpdateService;

    /**
     * MovieController constructor.
     * @param IMovieStoreService $movieStoreService
     * @param IMovieService $movieService
     * @param IMovieDeleteService $movieDeleteService
     * @param IMovieUpdateService $movieUpdateService
     */
    public function __construct(
        IMovieStoreService $movieStoreService,
        IMovieService $movieService,
        IMovieDeleteService $movieDeleteService,
        IMovieUpdateService $movieUpdateService
    ) {
        $this->movieStoreService = $movieStoreService;
        $this->movieService = $movieService;
        $this->movieDeleteService = $movieDeleteService;
        $this->movieUpdateService = $movieUpdateService;
    }

    /**
     * @param MovieSearchRequest $movieSearchRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(MovieSearchRequest $movieSearchRequest)
    {
        $movieSearchModel = $movieSearchRequest->map();
        try {
            $results = $this->movieService->get($movieSearchModel);
            $mappedResults = SearchModelMapper::toArray(
                $results,
                (new SearchMovieModelMapper())
            );

            return JsonResponse::ok($mappedResults, 200);
        } catch (NoMoviesAvailableException $noMoviesAvailableException) {
            return JsonResponse::notOk(
                $noMoviesAvailableException->getMessage(),
                $noMoviesAvailableException->getCode()
            );
        } catch (\Exception $exception) {
            return JsonResponse::notOkException($exception);
        }
    }

    /**
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function findById(string $id)
    {
        try {
            $result = $this->movieService->findById($id);
            $mappedResult = MovieModelMapper::toArray($result);
            return JsonResponse::ok($mappedResult);
        } catch (\Exception $exception) {
            return JsonResponse::notOkException($exception);
        }
    }

    /**
     * @param MovieStoreRequest $movieStoreRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(MovieStoreRequest $movieStoreRequest)
    {
        $movieModel = $movieStoreRequest->map();
        try {
            $result = $this->movieStoreService->store($movieModel);
            $mappedResult = MovieModelMapper::toArray($result);
            return JsonResponse::ok($mappedResult, 200);
        } catch (\Exception $exception) {
            return JsonResponse::notOkException($exception);
        }
    }

    /**
     * @param MovieStoreCollectionRequest $movieStoreCollectionRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeCollection(MovieStoreCollectionRequest $movieStoreCollectionRequest)
    {
        $movieCollectionModel = $movieStoreCollectionRequest->map();

        try {
            $collectionResults = $this->movieStoreService->storeCollection($movieCollectionModel);
            $mappedCollectionResult = MovieModelMapper::toCollection($collectionResults);
            return JsonResponse::ok($mappedCollectionResult);
        } catch (\Exception $exception) {
            return JsonResponse::notOkException($exception);
        }
    }

    /**
     * @param MovieUpdateRequest $movieUpdateRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(MovieUpdateRequest $movieUpdateRequest)
    {
        $movieModel = $movieUpdateRequest->map();

        try {
            $result = $this->movieUpdateService->update($movieModel);
            $mappedResult = MovieModelMapper::toArray($result);
            return JsonResponse::ok(
                [
                    'message' => 'movie was updated',
                    $mappedResult
                ],
                200
            );
        } catch (MovieNotFoundException $movieNotFoundException) {
            return JsonResponse::notOk(
                $movieNotFoundException->getMessage(),
                $movieNotFoundException->getCode()
            );
        } catch (\Exception $exception) {
            return JsonResponse::notOkException($exception);
        }
    }

    /**
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(string $id)
    {
        try {
            $this->movieDeleteService->delete($id);
            return JsonResponse::ok([
                'message' => 'Movie has been removed from the records'
            ], 202);
        } catch (MovieNotFoundException $movieNotFoundException) {
            return JsonResponse::notOk(
                $movieNotFoundException->getMessage(),
                $movieNotFoundException->getCode()
            );
        } catch (\Exception $exception) {
            return JsonResponse::notOkException($exception);
        }
    }
}
