<?php

namespace App\Http\Controllers\Genre;

use App\Exceptions\Genre\GenreNotFoundException;
use App\Exceptions\Genre\NoGenresAvailableException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Genre\GenreSearchRequest;
use App\Http\Requests\Genre\GenreStoreRequest;
use App\Http\Requests\Genre\GenreUpdateRequest;
use App\Src\Mappers\Genre\GenreModelMapper;
use App\Src\Mappers\Search\Genre\SearchGenreModelMapper;
use App\Src\Mappers\Search\SearchModelMapper;
use App\Src\Responses\JsonResponse;
use App\Src\Services\Genre\IGenreDeleteService;
use App\Src\Services\Genre\IGenreService;
use App\Src\Services\Genre\IGenreStoreService;
use App\Src\Services\Genre\IGenreUpdateService;
use Illuminate\Http\Request;

class GenreController extends Controller
{
    /**
     * @var IGenreService
     */
    private $genreService;
    /**
     * @var IGenreStoreService
     */
    private $genreStoreService;

    /**
     * @var IGenreUpdateService
     */
    private $genreUpdateService;

    /**
     * @var IGenreDeleteService
     */
    private $genreDeleteService;

    /**
     * GenreController constructor.
     * @param IGenreStoreService $genreStoreService
     * @param IGenreUpdateService $genreUpdateService
     * @param IGenreDeleteService $genreDeleteService
     * @param IGenreService $genreService
     */
    public function __construct(
        IGenreStoreService $genreStoreService,
        IGenreUpdateService $genreUpdateService,
        IGenreDeleteService $genreDeleteService,
        IGenreService $genreService
    ) {
        $this->genreStoreService = $genreStoreService;
        $this->genreUpdateService = $genreUpdateService;
        $this->genreDeleteService = $genreDeleteService;
        $this->genreService = $genreService;
    }

    /**
     * @param GenreSearchRequest $genreSearchRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(GenreSearchRequest $genreSearchRequest)
    {
        $genreSearchModel = $genreSearchRequest->map();
        try {
            $results = $this->genreService->get($genreSearchModel);
            $mappedResults = SearchModelMapper::toArray(
                $results,
                (new SearchGenreModelMapper())
            );
            return JsonResponse::ok($mappedResults, 200);
        } catch (NoGenresAvailableException $noGenresAvailableException) {
            return JsonResponse::notOk(
                $noGenresAvailableException->getMessage(),
                $noGenresAvailableException->getCode()
            );
        } catch (\Exception $exception) {
            return JsonResponse::notOkException($exception);
        }
    }

    /**
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function findById(string $id)
    {
        try {
            $result = $this->genreService->findById($id);
            $mappedResult = GenreModelMapper::toArray($result);
            return JsonResponse::ok($mappedResult);
        } catch (GenreNotFoundException $genreNotFoundException) {
            return JsonResponse::notOk(
                $genreNotFoundException->getMessage(),
                $genreNotFoundException->getCode()
            );
        } catch (\Exception $exception) {
            return JsonResponse::notOkException($exception);
        }
    }

    /**
     * @param GenreStoreRequest $genreStoreRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(GenreStoreRequest $genreStoreRequest)
    {
        $genreModel = $genreStoreRequest->map();
        try {
            $result = $this->genreStoreService->store($genreModel);
            $mappedResult = GenreModelMapper::toArray($result);
            return JsonResponse::ok($mappedResult);
        } catch (\Exception $exception) {
            return JsonResponse::notOkException($exception);
        }
    }

    /**
     * @param GenreUpdateRequest $genreUpdateRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(GenreUpdateRequest $genreUpdateRequest)
    {
        $genreModel = $genreUpdateRequest->map();
        try {
            $result = $this->genreUpdateService->update($genreModel);
            $mappedResult = GenreModelMapper::toArray($result);
            return JsonResponse::ok([
                'message' => 'genre was updated',
                $mappedResult
            ], 200);
        } catch (GenreNotFoundException $genreNotFoundException) {
            return JsonResponse::notOk(
                $genreNotFoundException->getMessage(),
                $genreNotFoundException->getCode()
            );
        } catch (\Exception $exception) {
            return JsonResponse::notOkException($exception);
        }
    }

    /**
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(string $id)
    {
        try {
            $this->genreDeleteService->delete($id);
            return JsonResponse::ok([
                'message' => 'Genre has been removed from the records'
            ], 202);
        } catch (GenreNotFoundException $genreNotFoundException) {
            return JsonResponse::notOk(
                $genreNotFoundException->getMessage(),
                $genreNotFoundException->getCode()
            );
        } catch (\Exception $exception) {
            return JsonResponse::notOkException($exception);
        }
    }
}
