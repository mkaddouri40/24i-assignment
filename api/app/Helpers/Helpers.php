<?php

if (!function_exists('keyExistsOrNull')) {
    function keyExistsOrNull($request, $key, $value)
    {
        return $request->has($key . '.' . $value) ? $request->get($key)[$value] : null;
    }
}

if (!function_exists('methodExistOrNull')) {
    function methodExistOrNull($class, string $method)
    {
        return ($class && method_exists($class, $method)) ? $class->{$method}() : null;
    }
}
