<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 28/07/2020
 * Time: 23:54
 */

namespace App\Src\Mappers\User;

use App\Models\User;
use App\Src\Models\User\UserModel;

class UserEloquentMapper
{
    /**
     * @param User $user
     * @return UserModel
     */
    public static function toModel(User $user)
    {
        return (new UserModel())
            ->setId($user->id)
            ->setName($user->name)
            ->setEmail($user->email)
            ->setPassword($user->password);
    }
}
