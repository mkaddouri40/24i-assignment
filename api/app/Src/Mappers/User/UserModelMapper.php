<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 28/07/2020
 * Time: 23:30
 */

namespace App\Src\Mappers\User;

use App\Models\User;
use App\Src\Models\User\UserModel;
use Illuminate\Support\Facades\Hash;

class UserModelMapper
{
    /**
     * @param UserModel $userModel
     * @return User
     */
    public static function toEloquentModel(UserModel $userModel)
    {
        $user = new User();
        $user->name = $userModel->getName();
        $user->email = $userModel->getEmail();
        $user->password = Hash::make($userModel->getPassword());

        return $user;
    }

    /**
     * @param UserModel $userModel
     * @return array
     */
    public static function toArray(UserModel $userModel)
    {
        return [
            'id' => $userModel->getId(),
            'name' => $userModel->getName(),
            'email' => $userModel->getEmail(),
            'email_verified_at' => $userModel->getEmailVerifiedAt(),
            'password' => $userModel->getPassword(),
            'remember_token' => $userModel->getRememberToken()
        ];
    }
}
