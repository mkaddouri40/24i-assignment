<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 24/07/2020
 * Time: 21:28
 */

namespace App\Src\Mappers\Search;

interface ISearchItemModelMapper
{
    /**
     * @param $model
     * @return mixed
     */
    public static function toArray($model);
}
