<?php

namespace App\Src\Mappers\Search;

use Illuminate\Support\Collection;

interface ISearchCollectionModelMapper
{
    /**
     * @param Collection $collection
     * @return mixed
     */
    public static function toCollectionArray(Collection $collection);
}
