<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 24/07/2020
 * Time: 21:29
 */

namespace App\Src\Mappers\Search;

use App\Src\Models\Search\SearchModel;

class SearchModelMapper
{
    /**
     * @param SearchModel $searchModel
     * @param ISearchCollectionModelMapper $ISearchCollectionModelMapper
     * @return array
     */
    public static function toArray(
        SearchModel $searchModel,
        ISearchCollectionModelMapper $ISearchCollectionModelMapper
    ) {
        return [
            'page' => $searchModel->getPage(),
            'total_pages' => $searchModel->getTotalPages(),
            'limit' => $searchModel->getLimit(),
            'results' => $ISearchCollectionModelMapper::toCollectionArray(
                $searchModel->getItems()
            ),
            'total_items' => $searchModel->getTotalItems()
        ];
    }
}
