<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 24/07/2020
 * Time: 22:16
 */

namespace App\Src\Mappers\Search\Movie;

use App\Src\Mappers\Movie\MovieModelMapper;
use App\Src\Mappers\Search\ISearchCollectionModelMapper;
use App\Src\Mappers\Search\ISearchItemModelMapper;
use Illuminate\Support\Collection;

class SearchMovieModelMapper implements ISearchItemModelMapper, ISearchCollectionModelMapper
{

    /**
     * @param Collection $collection
     * @return mixed
     */
    public static function toCollectionArray(Collection $collection)
    {
        return $collection->map(
            function ($item) {
                return self::toArray($item);
            }
        );
    }

    /**
     * @param $model
     * @return mixed
     */
    public static function toArray($model)
    {
        return MovieModelMapper::toArray($model);
    }
}
