<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 27/07/2020
 * Time: 01:40
 */

namespace App\Src\Mappers\Search\Genre;

use App\Src\Mappers\Genre\GenreModelMapper;
use App\Src\Mappers\Search\ISearchCollectionModelMapper;
use App\Src\Mappers\Search\ISearchItemModelMapper;
use Illuminate\Support\Collection;

class SearchGenreModelMapper implements ISearchItemModelMapper, ISearchCollectionModelMapper
{

    /**
     * @param Collection $collection
     * @return mixed
     */
    public static function toCollectionArray(Collection $collection)
    {
        return $collection->map(
            function ($item) {
                return self::toArray($item);
            }
        );
    }

    /**
     * @param $model
     * @return mixed
     */
    public static function toArray($model)
    {
        return GenreModelMapper::toArray($model);
    }
}
