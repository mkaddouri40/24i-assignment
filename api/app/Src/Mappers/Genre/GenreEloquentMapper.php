<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 26/07/2020
 * Time: 19:59
 */

namespace App\Src\Mappers\Genre;

use App\Models\Genre;
use App\Src\Models\Genre\GenreModel;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class GenreEloquentMapper
{
    /**
     * @param Collection $collection
     * @return Collection
     */
    public static function toCollection(Collection $collection)
    {
        return $collection->map(function ($item) {
            return self::toModel($item);
        });
    }

    /**
     * @param Genre $genre
     * @return GenreModel
     */
    public static function toModel(Genre $genre)
    {
        return (new GenreModel())
            ->setId((string)$genre->id)
            ->setName($genre->name)
            ->setCreatedAt(new Carbon($genre->created_at))
            ->setUpdatedAt(new Carbon($genre->updated_at));
    }
}
