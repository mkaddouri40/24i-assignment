<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 26/07/2020
 * Time: 20:03
 */

namespace App\Src\Mappers\Genre;

use App\Models\Genre;
use App\Src\Models\Genre\GenreModel;
use Illuminate\Support\Collection;

class GenreModelMapper
{
    /**
     * @param Collection $collection
     * @return array
     */
    public static function toCollection(Collection $collection)
    {
        return $collection->map(function ($item) {
            return self::toArray($item);
        })->toArray();
    }

    /**
     * @param GenreModel $genreModel
     * @return Genre
     */
    public static function toEloquentModel(GenreModel $genreModel)
    {
        $genre = new Genre();
        $genre->id = $genreModel->getId();
        $genre->name = $genreModel->getName();

        return $genre;
    }

    /**
     * @param GenreModel $orgModel
     * @param GenreModel $updateModel
     * @return Genre
     */
    public static function toEloquentUpdateModel(GenreModel $orgModel, GenreModel $updateModel)
    {
        $genre = new Genre();
        $genre->id = $orgModel->getId();
        $genre->name = $updateModel->getName() ?? $orgModel->getName();

        return $genre;
    }

    /**
     * @param GenreModel $genreModel
     * @return array
     */
    public static function toArray(GenreModel $genreModel)
    {
        return [
            'id' => $genreModel->getId(),
            'name' => $genreModel->getName(),
            'created_at' => $genreModel->getCreatedAt()->toDateTime(),
            'updated_at' => $genreModel->getUpdatedAt()->toDateTime()
        ];
    }
}
