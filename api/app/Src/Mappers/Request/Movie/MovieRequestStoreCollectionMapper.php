<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 27/07/2020
 * Time: 23:55
 */

namespace App\Src\Mappers\Request\Movie;

use App\Http\Requests\Movie\MovieStoreCollectionRequest;
use App\Src\Models\Movie\MovieModel;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class MovieRequestStoreCollectionMapper
{
    /**
     * @param Collection $collection
     * @return Collection
     */
    public static function toCollection(Collection $collection)
    {
        return $collection->map(function ($item) {
            dump($item);
        });
    }

    /**
     * @param MovieStoreCollectionRequest $movieStoreCollectionRequest
     * @return Collection
     */
    public static function toMovieModel(MovieStoreCollectionRequest $movieStoreCollectionRequest)
    {
        $collection = new Collection();
        foreach ($movieStoreCollectionRequest->movies as $movie) {
            $collection->add(
                (new MovieModel())
                    ->setId(Str::uuid())
                    ->setName($movie['name'])
                    ->setGenreIds($movie['genres'])
            );
        }
        return $collection;
    }
}
