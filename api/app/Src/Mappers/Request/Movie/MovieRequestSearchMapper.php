<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 24/07/2020
 * Time: 21:52
 */

namespace App\Src\Mappers\Request\Movie;

use App\Http\Requests\Movie\MovieSearchRequest;
use App\Src\Models\Movie\MovieModel;
use App\Src\Models\Search\SearchModel;

class MovieRequestSearchMapper
{
    /**
     * @param MovieSearchRequest $movieSearchRequest
     * @return mixed
     */
    public static function toModel(MovieSearchRequest $movieSearchRequest)
    {
        return (new SearchModel())
            ->setOrderBy($movieSearchRequest->order_by)
            ->setDirection($movieSearchRequest->direction)
            ->setPage($movieSearchRequest->page)
            ->setLimit($movieSearchRequest->limit)
            ->setMovie(
                (new MovieModel())
                    ->setId((string)keyExistsOrNull($movieSearchRequest, 'search', 'id'))
                    ->setName((string)keyExistsOrNull($movieSearchRequest, 'search', 'name'))
            );
    }
}
