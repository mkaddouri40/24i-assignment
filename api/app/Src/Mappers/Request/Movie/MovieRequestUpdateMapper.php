<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 26/07/2020
 * Time: 02:52
 */

namespace App\Src\Mappers\Request\Movie;

use App\Http\Requests\Movie\MovieUpdateRequest;
use App\Src\Models\Movie\MovieModel;

class MovieRequestUpdateMapper
{
    /**
     * @param MovieUpdateRequest $movieUpdateRequest
     * @return MovieModel
     */
    public static function toModel(MovieUpdateRequest $movieUpdateRequest)
    {
        return (new MovieModel())
            ->setId($movieUpdateRequest->id)
            ->setName($movieUpdateRequest->name)
            ->setGenreIds($movieUpdateRequest->genres_add)
            ->setGenreIdsToRemove($movieUpdateRequest->genres_remove);
    }
}
