<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 24/07/2020
 * Time: 16:41
 */

namespace App\Src\Mappers\Request\Movie;

use App\Http\Requests\Movie\MovieStoreRequest;
use App\Src\Models\Movie\MovieModel;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class MovieRequestStoreMapper
{
    /**
     * @param MovieStoreRequest $movieStoreRequest
     * @return MovieModel
     */
    public static function toMovieModel(MovieStoreRequest $movieStoreRequest)
    {

        return (new MovieModel())
            ->setId(Str::uuid())
            ->setName($movieStoreRequest->name)
            ->setGenreIds($movieStoreRequest->genres);
    }
}
