<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 28/07/2020
 * Time: 23:27
 */

namespace App\Src\Mappers\Request\User;

use App\Http\Requests\User\UserStoreRequest;
use App\Src\Models\User\UserModel;

class UserStoreRequestMapper
{
    /**
     * @param UserStoreRequest $userStoreRequest
     * @return UserModel
     */
    public static function toModel(UserStoreRequest $userStoreRequest)
    {
        return (new UserModel())
            ->setName($userStoreRequest->name)
            ->setEmail($userStoreRequest->email)
            ->setPassword($userStoreRequest->password);
    }
}
