<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 29/07/2020
 * Time: 02:00
 */

namespace App\Src\Mappers\Request\User;

use App\Http\Requests\User\UserAuthRequest;
use App\Src\Models\Auth\AuthModel;

class UserAuthRequestMapper
{
    /**
     * @param UserAuthRequest $userAuthRequest
     * @return AuthModel
     */
    public static function toAuthModel(UserAuthRequest $userAuthRequest)
    {
        return (new AuthModel())
            ->setEmail($userAuthRequest->email)
            ->setPassword($userAuthRequest->password)
            ->setUserRequest($userAuthRequest);
    }
}
