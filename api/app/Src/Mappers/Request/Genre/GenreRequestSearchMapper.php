<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 27/07/2020
 * Time: 01:28
 */

namespace App\Src\Mappers\Request\Genre;

use App\Http\Requests\Genre\GenreSearchRequest;
use App\Src\Models\Genre\GenreModel;
use App\Src\Models\Search\SearchModel;

class GenreRequestSearchMapper
{
    /**
     * @param GenreSearchRequest $genreSearchRequest
     * @return mixed
     */
    public static function toModel(GenreSearchRequest $genreSearchRequest)
    {
        return (new SearchModel())
            ->setOrderBy($genreSearchRequest->order_by)
            ->setDirection($genreSearchRequest->direction)
            ->setPage($genreSearchRequest->page)
            ->setLimit($genreSearchRequest->limit)
            ->setGenre(
                (new GenreModel())
                    ->setId((string)keyExistsOrNull($genreSearchRequest, 'search', 'id'))
                    ->setName((string)keyExistsOrNull($genreSearchRequest, 'search', 'name'))
            );
    }
}
