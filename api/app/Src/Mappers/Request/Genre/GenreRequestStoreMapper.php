<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 26/07/2020
 * Time: 20:09
 */

namespace App\Src\Mappers\Request\Genre;

use App\Http\Requests\Genre\GenreStoreRequest;
use App\Src\Models\Genre\GenreModel;
use Illuminate\Support\Str;

class GenreRequestStoreMapper
{
    /**
     * @param GenreStoreRequest $genreStoreRequest
     * @return GenreModel
     */
    public static function toModel(GenreStoreRequest $genreStoreRequest)
    {
        return (new GenreModel())
            ->setId(Str::uuid())
            ->setName($genreStoreRequest->name);
    }
}
