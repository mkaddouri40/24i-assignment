<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 27/07/2020
 * Time: 00:13
 */

namespace App\Src\Mappers\Request\Genre;

use App\Http\Requests\Genre\GenreUpdateRequest;
use App\Src\Models\Genre\GenreModel;

class GenreRequestUpdateMapper
{
    /**
     * @param GenreUpdateRequest $genreUpdateRequest
     * @return GenreModel
     */
    public static function toModel(GenreUpdateRequest $genreUpdateRequest)
    {
        return (new GenreModel())
            ->setId($genreUpdateRequest->id)
            ->setName($genreUpdateRequest->name);
    }
}
