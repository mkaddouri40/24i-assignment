<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 24/07/2020
 * Time: 16:56
 */

namespace App\Src\Mappers\Movie;

use App\Models\Movie;
use App\Src\Mappers\Genre\GenreModelMapper;
use App\Src\Models\Movie\MovieModel;
use Illuminate\Support\Collection;

class MovieModelMapper
{
    /**
     * @param Collection $collection
     * @return array
     */
    public static function toCollection(Collection $collection)
    {
        return $collection->map(function ($item) {
            return self::toArray($item);
        })->toArray();
    }

    /**
     * @param MovieModel $movieModel
     * @return Movie
     */
    public static function toEloquentModel(MovieModel $movieModel)
    {
        $movie = new Movie();
        $movie->id = $movieModel->getId();
        $movie->name = $movieModel->getName();

        return $movie;
    }

    /**
     * @param MovieModel $orgModel
     * @param MovieModel $updateModel
     * @return Movie
     */
    public static function toEloquentUpdateModel(MovieModel $orgModel, MovieModel $updateModel)
    {
        $movie = new Movie();
        $movie->id = $orgModel->getId();
        $movie->name = $updateModel->getName() ?? $orgModel->getName();

        return $movie;
    }

    /**
     * @param MovieModel $movieModel
     * @return array
     */
    public static function toArray(MovieModel $movieModel)
    {
        $genres = $movieModel->getGenres();

        if ($genres) {
            $genres = GenreModelMapper::toCollection($genres);
        }

        return [
            'id' => $movieModel->getId(),
            'name' => $movieModel->getName(),
            'created_at' => $movieModel->getCreatedAt()->toDateTime(),
            'updated_at' => $movieModel->getUpdatedAt()->toDateTime(),
            'genres' => $genres
        ];
    }
}
