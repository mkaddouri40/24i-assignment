<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 24/07/2020
 * Time: 17:03
 */

namespace App\Src\Mappers\Movie;

use App\Models\Movie;
use App\Src\Mappers\Genre\GenreEloquentMapper;
use App\Src\Models\Movie\MovieModel;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class MovieEloquentMapper
{
    /**
     * @param Collection $collection
     * @return Collection
     */
    public static function toCollection(Collection $collection): Collection
    {
        return $collection->map(function ($item) {
            return self::toModel($item);
        });
    }

    /**
     * @param Movie $movie
     * @return MovieModel
     */
    public static function toModel(Movie $movie)
    {
        $genres = $movie->genres;

        if ($genres) {
            $genres = GenreEloquentMapper::toCollection($genres);
        }

        return (new MovieModel())
            ->setId((string)$movie->id)
            ->setName($movie->name)
            ->setCreatedAt(new Carbon($movie->created_at))
            ->setUpdatedAt(new Carbon($movie->updated_at))
            ->setGenres($genres);
    }
}
