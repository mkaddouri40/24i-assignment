<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 24/07/2020
 * Time: 22:05
 */

namespace App\Src\Services\Movie;

use App\Exceptions\Movie\MovieNotFoundException;
use App\Exceptions\Movie\NoMoviesAvailableException;
use App\Src\Models\Search\SearchModel;
use App\Src\Repositories\Movie\IMovieRepository;

class MovieService implements IMovieService
{

    /**
     * @var IMovieRepository
     */
    private $movieRepository;

    /**
     * MovieService constructor.
     * @param IMovieRepository $movieRepository
     */
    public function __construct(IMovieRepository $movieRepository)
    {
        $this->movieRepository = $movieRepository;
    }


    /**
     * @param SearchModel $searchModel
     * @return mixed
     * @throws NoMoviesAvailableException
     */
    public function get(SearchModel $searchModel)
    {
        $movies = $this->movieRepository->get($searchModel);

        if (!$movies) {
            throw new NoMoviesAvailableException();
        }

        return $movies;
    }

    /**
     * @param string $id
     * @return mixed
     * @throws MovieNotFoundException
     */
    public function findById(string $id)
    {
        $foundMovie = $this->movieRepository->findById($id);

        if (!$foundMovie) {
            throw new MovieNotFoundException();
        }

        return $foundMovie;
    }
}
