<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 24/07/2020
 * Time: 16:16
 */

namespace App\Src\Services\Movie;

use App\Src\Models\Movie\MovieModel;
use Illuminate\Support\Collection;

interface IMovieStoreService
{
    /**
     * @param MovieModel $movieModel
     * @return mixed
     */
    public function store(MovieModel $movieModel);

    /**
     * @param Collection $collection
     * @return mixed
     */
    public function storeCollection(Collection $collection);
}
