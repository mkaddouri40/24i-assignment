<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 25/07/2020
 * Time: 02:41
 */

namespace App\Src\Services\Movie;

use App\Exceptions\Movie\MovieNotFoundException;
use App\Src\Repositories\Movie\IMovieRepository;

class MovieDeleteService implements IMovieDeleteService
{
    /**
     * @var IMovieRepository
     */
    private $movieRepository;

    /**
     * @var IMovieService
     */
    private $movieService;

    /**
     * MovieDeleteService constructor.
     * @param IMovieRepository $movieRepository
     * @param IMovieService $movieService
     */
    public function __construct(IMovieRepository $movieRepository, IMovieService $movieService)
    {
        $this->movieRepository = $movieRepository;
        $this->movieService = $movieService;
    }


    /**
     * @param string $id
     * @return mixed
     */
    public function delete(string $id)
    {
        $this->movieService->findById($id);

        return $this->movieRepository->delete($id);
    }
}
