<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 24/07/2020
 * Time: 22:05
 */

namespace App\Src\Services\Movie;

use App\Src\Models\Search\SearchModel;

interface IMovieService
{
    /**
     * @param SearchModel $searchModel
     * @return mixed
     */
    public function get(SearchModel $searchModel);

    /**
     * @param string $id
     * @return mixed
     */
    public function findById(string $id);
}
