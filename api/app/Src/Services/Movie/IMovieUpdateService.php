<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 26/07/2020
 * Time: 02:59
 */

namespace App\Src\Services\Movie;

use App\Src\Models\Movie\MovieModel;

interface IMovieUpdateService
{
    /**
     * @param MovieModel $movieModel
     * @return mixed
     */
    public function update(MovieModel $movieModel);
}
