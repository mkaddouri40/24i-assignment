<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 26/07/2020
 * Time: 02:59
 */

namespace App\Src\Services\Movie;

use App\Src\Models\Movie\MovieModel;
use App\Src\Repositories\Movie\IMovieRepository;
use App\Src\Services\GenreMovie\IGenreMovieUpdateService;

class MovieUpdateService implements IMovieUpdateService
{

    /**
     * @var IMovieRepository
     */
    private $movieRepository;

    /**
     * @var IMovieService
     */
    private $movieService;

    /**
     * @var IGenreMovieUpdateService
     */
    private $genreMovieUpdateService;

    /**
     * MovieUpdateService constructor.
     * @param IMovieRepository $movieRepository
     * @param IMovieService $movieService
     * @param IGenreMovieUpdateService $genreMovieUpdateService
     */
    public function __construct(
        IMovieRepository $movieRepository,
        IMovieService $movieService,
        IGenreMovieUpdateService $genreMovieUpdateService
    ) {
        $this->movieRepository = $movieRepository;
        $this->movieService = $movieService;
        $this->genreMovieUpdateService = $genreMovieUpdateService;
    }


    /**
     * @param MovieModel $movieModel
     * @return mixed
     */
    public function update(MovieModel $movieModel)
    {
        $this->movieService->findById($movieModel->getId());
        $this->movieRepository->update($movieModel);
        return $this->genreMovieUpdateService->update($movieModel);
    }
}
