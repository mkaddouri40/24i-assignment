<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 25/07/2020
 * Time: 02:41
 */

namespace App\Src\Services\Movie;

interface IMovieDeleteService
{
    /**
     * @param string $id
     * @return mixed
     */
    public function delete(string $id);
}
