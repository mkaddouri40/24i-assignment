<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 24/07/2020
 * Time: 16:16
 */

namespace App\Src\Services\Movie;

use App\Src\Models\Movie\MovieModel;
use App\Src\Repositories\Movie\IMovieRepository;
use App\Src\Services\GenreMovie\IGenreMovieStoreService;
use Illuminate\Support\Collection;

class MovieStoreService implements IMovieStoreService
{

    /**
     * @var IMovieRepository
     */
    private $movieRepository;

    /**
     * @var IGenreMovieStoreService
     */
    private $genreMovieStoreService;

    /**
     * MovieStoreService constructor.
     * @param IMovieRepository $movieRepository
     * @param IGenreMovieStoreService $genreMovieStoreService
     */
    public function __construct(
        IMovieRepository $movieRepository,
        IGenreMovieStoreService $genreMovieStoreService
    ) {
        $this->movieRepository = $movieRepository;
        $this->genreMovieStoreService = $genreMovieStoreService;
    }

    /**
     * @param MovieModel $movieModel
     * @return mixed
     */
    public function store(MovieModel $movieModel)
    {
        $storeMovieModel = $this->movieRepository->store($movieModel);
        $storeMovieModel->setGenreIds($movieModel->getGenreIds());
        return $this->genreMovieStoreService->store($storeMovieModel);
    }

    /**
     * @param Collection $collection
     * @return Collection|mixed
     */
    public function storeCollection(Collection $collection)
    {
        $newCollection = new Collection();
        $collection->each(function (MovieModel $movieModel) use ($newCollection) {
            $movieStoredModel = $this->store($movieModel);
            $newCollection->add($movieStoredModel);
        });

        return $newCollection;
    }
}
