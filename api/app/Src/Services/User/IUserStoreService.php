<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 28/07/2020
 * Time: 23:22
 */

namespace App\Src\Services\User;

use App\Src\Models\User\UserModel;

interface IUserStoreService
{
    /**
     * @param UserModel $userModel
     * @return UserModel
     */
    public function store(UserModel $userModel): UserModel;
}
