<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 28/07/2020
 * Time: 23:22
 */

namespace App\Src\Services\User;

use App\Src\Models\User\UserModel;
use App\Src\Repositories\User\IUserRepository;

class UserStoreService implements IUserStoreService
{
    /**
     * @var IUserRepository
     */
    private $userRepository;

    /**
     * @var IUserService
     */
    private $userService;

    /**
     * UserStoreService constructor.
     * @param IUserRepository $userRepository
     * @param IUserService $userService
     */
    public function __construct(IUserRepository $userRepository, IUserService $userService)
    {
        $this->userRepository = $userRepository;
        $this->userService = $userService;
    }


    /**
     * @param UserModel $userModel
     * @return UserModel
     */
    public function store(UserModel $userModel): UserModel
    {
        $this->userService->findByEmail($userModel->getEmail());
        return $this->userRepository->store($userModel);
    }
}
