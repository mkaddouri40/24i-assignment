<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 29/07/2020
 * Time: 00:03
 */

namespace App\Src\Services\User;

use App\Exceptions\User\EmailAllReadyExistsException;
use App\Src\Repositories\User\IUserRepository;

class UserService implements IUserService
{

    /**
     * @var IUserRepository
     */
    private $userRepository;

    /**
     * CustomerService constructor.
     * @param IUserRepository $userRepository
     */
    public function __construct(IUserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $email
     * @return mixed
     * @throws EmailAllReadyExistsException
     */
    public function findByEmail(string $email)
    {
        $userFound = $this->userRepository->findByEmail($email);

        if ($userFound) {
            throw new EmailAllReadyExistsException();
        }
    }

    /**
     * @param string $email
     * @return mixed
     */
    public function checkEmailExists(string $email)
    {
        $userFound = $this->userRepository->findByEmail($email);

        if ($userFound) {
            return true;
        }

        return false;
    }
}
