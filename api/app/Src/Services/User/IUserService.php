<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 29/07/2020
 * Time: 00:04
 */

namespace App\Src\Services\User;

interface IUserService
{
    /**
     * @param string $email
     * @return mixed
     */
    public function findByEmail(string $email);

    /**
     * @param string $email
     * @return mixed
     */
    public function checkEmailExists(string $email);
}
