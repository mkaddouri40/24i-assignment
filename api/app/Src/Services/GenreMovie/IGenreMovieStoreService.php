<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 27/07/2020
 * Time: 19:05
 */

namespace App\Src\Services\GenreMovie;

use App\Src\Models\Movie\MovieModel;

interface IGenreMovieStoreService
{
    /**
     * @param MovieModel $movieModel
     * @return mixed
     */
    public function store(MovieModel $movieModel);
}
