<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 27/07/2020
 * Time: 19:06
 */

namespace App\Src\Services\GenreMovie;

use App\Src\Models\Movie\MovieModel;
use App\Src\Repositories\GenreMovie\IGenreMovieRepository;

class GenreMovieStoreService implements IGenreMovieStoreService
{
    /**
     * @var IGenreMovieRepository
     */
    private $genreMovieRepository;

    /**
     * GenreMovieStoreService constructor.
     * @param IGenreMovieRepository $genreMovieRepository
     */
    public function __construct(IGenreMovieRepository $genreMovieRepository)
    {
        $this->genreMovieRepository = $genreMovieRepository;
    }


    /**
     * @param MovieModel $movieModel
     * @return mixed
     */
    public function store(MovieModel $movieModel)
    {
        return $this->genreMovieRepository->store($movieModel);
    }
}
