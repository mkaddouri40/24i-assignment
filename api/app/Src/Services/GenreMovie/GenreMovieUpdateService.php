<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 28/07/2020
 * Time: 05:41
 */

namespace App\Src\Services\GenreMovie;

use App\Src\Models\Movie\MovieModel;
use App\Src\Repositories\GenreMovie\IGenreMovieRepository;

class GenreMovieUpdateService implements IGenreMovieUpdateService
{
    /**
     * @var IGenreMovieRepository
     */
    private $genreMovieRepository;

    /**
     * GenreMovieUpdateService constructor.
     * @param IGenreMovieRepository $genreMovieRepository
     */
    public function __construct(IGenreMovieRepository $genreMovieRepository)
    {
        $this->genreMovieRepository = $genreMovieRepository;
    }


    /**
     * @param MovieModel $movieModel
     * @return mixed
     */
    public function update(MovieModel $movieModel)
    {
        return $this->genreMovieRepository->update($movieModel);
    }
}
