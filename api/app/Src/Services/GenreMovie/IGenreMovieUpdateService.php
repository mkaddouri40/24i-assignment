<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 28/07/2020
 * Time: 05:41
 */

namespace App\Src\Services\GenreMovie;

use App\Src\Models\Movie\MovieModel;

interface IGenreMovieUpdateService
{
    /**
     * @param MovieModel $movieModel
     * @return mixed
     */
    public function update(MovieModel $movieModel);
}
