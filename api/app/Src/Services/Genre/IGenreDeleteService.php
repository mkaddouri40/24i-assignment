<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 27/07/2020
 * Time: 01:05
 */

namespace App\Src\Services\Genre;

interface IGenreDeleteService
{
    /**
     * @param string $id
     * @return mixed
     */
    public function delete(string $id);
}
