<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 27/07/2020
 * Time: 00:06
 */

namespace App\Src\Services\Genre;

use App\Src\Models\Genre\GenreModel;

interface IGenreUpdateService
{
    /**
     * @param GenreModel $genreModel
     * @return mixed
     */
    public function update(GenreModel $genreModel);
}
