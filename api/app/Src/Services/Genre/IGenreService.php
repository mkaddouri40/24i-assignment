<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 27/07/2020
 * Time: 00:20
 */

namespace App\Src\Services\Genre;

use App\Src\Models\Search\SearchModel;

interface IGenreService
{
    /**
     * @param SearchModel $searchModel
     * @return mixed
     */
    public function get(SearchModel $searchModel);

    /**
     *
     * @param string $id
     * @return mixed
     */
    public function findById(string $id);
}
