<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 26/07/2020
 * Time: 23:13
 */

namespace App\Src\Services\Genre;

use App\Src\Models\Genre\GenreModel;

interface IGenreStoreService
{
    /**
     * @param GenreModel $genreModel
     * @return mixed
     */
    public function store(GenreModel $genreModel);
}
