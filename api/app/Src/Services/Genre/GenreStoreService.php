<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 26/07/2020
 * Time: 23:12
 */

namespace App\Src\Services\Genre;

use App\Src\Models\Genre\GenreModel;
use App\Src\Repositories\Genre\IGenreRepository;

class GenreStoreService implements IGenreStoreService
{
    /**
     * @var IGenreRepository
     */
    private $genreRepository;

    /**
     * GenreStoreService constructor.
     * @param IGenreRepository $genreRepository
     */
    public function __construct(IGenreRepository $genreRepository)
    {
        $this->genreRepository = $genreRepository;
    }


    /**
     * @param GenreModel $genreModel
     * @return mixed
     */
    public function store(GenreModel $genreModel)
    {
        return $this->genreRepository->store($genreModel);
    }
}
