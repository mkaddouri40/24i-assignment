<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 27/07/2020
 * Time: 00:07
 */

namespace App\Src\Services\Genre;

use App\Src\Models\Genre\GenreModel;
use App\Src\Repositories\Genre\IGenreRepository;

class GenreUpdateService implements IGenreUpdateService
{
    /**
     * @var IGenreRepository
     */
    private $genreRepository;

    /**
     * @var IGenreService
     */
    private $genreService;

    /**
     * GenreUpdateService constructor.
     * @param IGenreRepository $genreRepository
     * @param IGenreService $genreService
     */
    public function __construct(IGenreRepository $genreRepository, IGenreService $genreService)
    {
        $this->genreRepository = $genreRepository;
        $this->genreService = $genreService;
    }


    /**
     * @param GenreModel $genreModel
     * @return mixed
     */
    public function update(GenreModel $genreModel)
    {
        $this->genreService->findById($genreModel->getId());
        return $this->genreRepository->update($genreModel);
    }
}
