<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 27/07/2020
 * Time: 00:20
 */

namespace App\Src\Services\Genre;

use App\Exceptions\Genre\GenreNotFoundException;
use App\Exceptions\Genre\NoGenresAvailableException;
use App\Src\Models\Search\SearchModel;
use App\Src\Repositories\Genre\IGenreRepository;

class GenreService implements IGenreService
{
    /**
     * @var IGenreRepository
     */
    private $genreRepository;

    /**
     * GenreService constructor.
     * @param IGenreRepository $genreRepository
     */
    public function __construct(IGenreRepository $genreRepository)
    {
        $this->genreRepository = $genreRepository;
    }

    /**
     * @param SearchModel $searchModel
     * @return mixed
     * @throws NoGenresAvailableException
     */
    public function get(SearchModel $searchModel)
    {
        $genres = $this->genreRepository->get($searchModel);

        if (!$genres) {
            throw new NoGenresAvailableException();
        }

        return $genres;
    }

    /**
     * @param string $id
     * @return mixed
     * @throws GenreNotFoundException
     */
    public function findById(string $id)
    {
        $foundGenre = $this->genreRepository->findById($id);

        if (!$foundGenre) {
            throw new GenreNotFoundException();
        }

        return $foundGenre;
    }
}
