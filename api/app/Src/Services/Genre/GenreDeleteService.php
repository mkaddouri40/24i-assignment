<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 27/07/2020
 * Time: 01:05
 */

namespace App\Src\Services\Genre;

use App\Src\Repositories\Genre\IGenreRepository;

class GenreDeleteService implements IGenreDeleteService
{
    /**
     * @var IGenreRepository
     */
    private $genreRepository;

    /**
     * @var IGenreService
     */
    private $genreService;

    /**
     * GenreDeleteService constructor.
     * @param IGenreRepository $genreRepository
     * @param IGenreService $genreService
     */
    public function __construct(IGenreRepository $genreRepository, IGenreService $genreService)
    {
        $this->genreRepository = $genreRepository;
        $this->genreService = $genreService;
    }


    /**
     * @param string $id
     * @return mixed
     */
    public function delete(string $id)
    {
        $this->genreService->findById($id);
        return $this->genreRepository->delete($id);
    }
}
