<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 26/07/2020
 * Time: 20:11
 */

namespace App\Src\Repositories\Genre;

use App\Src\Models\Genre\GenreModel;
use App\Src\Models\Search\SearchModel;

interface IGenreRepository
{
    /**
     * @param SearchModel $searchModel
     * @return mixed
     */
    public function get(SearchModel $searchModel);
    /**
     * @param GenreModel $genreModel
     * @return mixed
     */
    public function store(GenreModel $genreModel);

    /**
     * @param GenreModel $genreModel
     * @return mixed
     */
    public function update(GenreModel $genreModel);

    /**
     * @param $column
     * @param $value
     * @param bool $eloquentModel
     * @return mixed
     */
    public function findBy($column, $value, bool $eloquentModel = false);

    /**
     * @param string $id
     * @param bool $eloquentModel
     * @return mixed
     */
    public function findById(string $id, bool $eloquentModel = false);

    /**
     * @param string $id
     * @return mixed
     */
    public function delete(string $id);
}
