<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 26/07/2020
 * Time: 20:11
 */

namespace App\Src\Repositories\Genre;

use App\Models\Genre;
use App\Src\Mappers\Genre\GenreEloquentMapper;
use App\Src\Mappers\Genre\GenreModelMapper;
use App\Src\Models\Genre\GenreModel;
use App\Src\Models\Search\SearchModel;

class GenreRepository implements IGenreRepository
{
    public function get(SearchModel $searchModel)
    {
        $genres = Genre::query()
            ->with(['movies'])
            ->tap(function ($collection) use ($searchModel) {
                $searchModel->setTotalItems($collection->count());
            });

        if ($searchModel->getLimit()) {
            $genres->limit($searchModel->getLimit())
                ->offset(
                    $searchModel->getLimit() * ($searchModel->getPage() - 1)
                );
        }

        $genres = $genres->orderBy($searchModel->getOrderBy(), $searchModel->getDirection());

        if ($searchModel->getGenre()->getName() !== "" || $searchModel->getDirection() !== null ||
            $searchModel->getGenre()->getId() !== "" || $searchModel->getGenre()->getId() !== null) {
            $genres->search($searchModel);
        }

        $genres = $genres->get();

        $genres = GenreEloquentMapper::toCollection($genres);
        return $searchModel->setItems($genres);
    }


    /**
     * @param GenreModel $genreModel
     * @return mixed
     */
    public function store(GenreModel $genreModel)
    {
        $genre = GenreModelMapper::toEloquentModel($genreModel);
        $genre->save();

        return GenreEloquentMapper::toModel($genre);
    }

    /**
     * @param GenreModel $genreModel
     * @return mixed
     */
    public function update(GenreModel $genreModel)
    {
        $foundGenre = $this->findById($genreModel->getId());
        $updatedGenre = GenreModelMapper::toEloquentUpdateModel($foundGenre, $genreModel);
        $updatedGenre->exists = true;
        $updatedGenre->save();

        return GenreEloquentMapper::toModel($updatedGenre);
    }

    /**
     * @param $column
     * @param $value
     * @param bool $eloquentModel
     * @return mixed
     */
    public function findBy($column, $value, bool $eloquentModel = false)
    {
        $genre = Genre::where($column, $value)->first();

        if (!$genre) {
            return null;
        }

        if ($eloquentModel) {
            return $genre;
        }

        return GenreEloquentMapper::toModel($genre);
    }

    /**
     * @param string $id
     * @param bool $eloquentModel
     * @return mixed
     */
    public function findById(string $id, bool $eloquentModel = false)
    {
        return $this->findBy('id', $id, $eloquentModel);
    }


    /**
     * @param string $id
     * @return mixed
     */
    public function delete(string $id)
    {
        $foundGenre = $this->findById($id, true);

        if (!$foundGenre) {
            return false;
        }

        $foundGenre->movies()->detach();

        return $foundGenre->delete($id);
    }
}
