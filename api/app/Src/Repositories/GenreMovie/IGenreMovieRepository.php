<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 27/07/2020
 * Time: 05:28
 */

namespace App\Src\Repositories\GenreMovie;

use App\Src\Models\Movie\MovieModel;

interface IGenreMovieRepository
{
    /**
     * @param MovieModel $movieModel
     * @return mixed
     */
    public function store(MovieModel $movieModel);

    /**
     * @param MovieModel $movieModel
     * @return mixed
     */
    public function update(MovieModel $movieModel);

    /**
     * @param $column
     * @param $value
     * @param bool $eloquentModel
     * @return mixed
     */
    public function findBy($column, $value, bool $eloquentModel = false);

    /**
     * @param string $id
     * @param bool $eloquentModel
     * @return mixed
     */
    public function findById(string $id, bool $eloquentModel = false);
}
