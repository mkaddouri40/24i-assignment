<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 27/07/2020
 * Time: 05:28
 */

namespace App\Src\Repositories\GenreMovie;

use App\Models\Genre;
use App\Models\Movie;
use App\Src\Mappers\Movie\MovieEloquentMapper;
use App\Src\Models\Movie\MovieModel;

class GenreMovieRepository implements IGenreMovieRepository
{

    /**
     * @param MovieModel $movieModel
     * @return mixed
     */
    public function store(MovieModel $movieModel)
    {
//        $chapter->courses()->attach($course->id, ['chapter_order_number' => $index]);
        $movie = $this->findById($movieModel->getId(), true);
        foreach ($movieModel->getGenreIds() as $genreId) {
            $movie->genres()->attach($movieModel->getId(), ['genre_id' => $genreId]);
        }

        return MovieEloquentMapper::toModel($movie);
    }

    /**
     * @param MovieModel $movieModel
     * @return MovieModel|mixed
     */
    public function update(MovieModel $movieModel)
    {
        $movie = $this->findById($movieModel->getId(), true);
        $movie->genres()->detach($movieModel->getGenreIdsToRemove());
        foreach ($movieModel->getGenreIds() as $genreId) {
            $movie->genres()->attach($movieModel->getId(), ['genre_id' => $genreId]);
        }

        return MovieEloquentMapper::toModel($movie);
    }


    /**
     * @param $column
     * @param $value
     * @param bool $eloquentModel
     * @return mixed
     */
    public function findBy($column, $value, bool $eloquentModel = false)
    {
        $movie = Movie::where($column, $value)->first();

        if (!$movie) {
            return null;
        }

        if ($eloquentModel) {
            return $movie;
        }

        return MovieEloquentMapper::toModel($movie);
    }

    /**
     * @param string $id
     * @param bool $eloquentModel
     * @return mixed
     */
    public function findById(string $id, bool $eloquentModel = false)
    {
        return $this->findBy('id', $id, $eloquentModel);
    }
}
