<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 24/07/2020
 * Time: 16:50
 */

namespace App\Src\Repositories\Movie;

use App\Models\Movie;
use App\Src\Mappers\Movie\MovieEloquentMapper;
use App\Src\Mappers\Movie\MovieModelMapper;
use App\Src\Models\Movie\MovieModel;
use App\Src\Models\Search\SearchModel;

class MovieRepository implements IMovieRepository
{
    /**
     * @param SearchModel $searchModel
     * @return SearchModel|mixed
     */
    public function get(SearchModel $searchModel)
    {
        $movies = Movie::query()
            ->with(['genres'])
            ->tap(function ($collection) use ($searchModel) {
                $searchModel->setTotalItems($collection->count());
            });

        if ($searchModel->getLimit()) {
            $movies->limit($searchModel->getLimit())
                ->offset(
                    $searchModel->getLimit() * ($searchModel->getPage() - 1)
                );
        }

        $movies = $movies->orderBy($searchModel->getOrderBy(), $searchModel->getDirection());

        if ($searchModel->getMovie()->getName() !== "" || $searchModel->getDirection() !== null ||
            $searchModel->getMovie()->getId() !== "" || $searchModel->getMovie()->getId() !== null) {
            $movies->search($searchModel);
        }

        $movies = $movies->get();

        $models = MovieEloquentMapper::toCollection($movies);
        return $searchModel->setItems($models);
    }

    /**
     * @param MovieModel $movieModel
     * @return mixed
     */
    public function store(MovieModel $movieModel): MovieModel
    {
        $movie = MovieModelMapper::toEloquentModel($movieModel);
        $movie->save();

        return MovieEloquentMapper::toModel($movie);
    }

    /**
     * @param MovieModel $movieModel
     * @return MovieModel|mixed
     */
    public function update(MovieModel $movieModel)
    {
        $foundMovie = $this->findById($movieModel->getId());
        $updatedMovie = MovieModelMapper::toEloquentUpdateModel($foundMovie, $movieModel);
        $updatedMovie->exists = true;
        $updatedMovie->save();

        return MovieEloquentMapper::toModel($updatedMovie);
    }

    /**
     * @param $column
     * @param $value
     * @param bool $eloquentModel
     * @return mixed
     */
    public function findBy($column, $value, bool $eloquentModel = false)
    {
        $movie = Movie::where($column, $value)->first();

        if (!$movie) {
            return null;
        }

        if ($eloquentModel) {
            return $movie;
        }

        return MovieEloquentMapper::toModel($movie);
    }

    /**
     * @param string $id
     * @param bool $eloquentModel
     * @return mixed
     */
    public function findById(string $id, bool $eloquentModel = false)
    {
        return $this->findBy('id', $id, $eloquentModel);
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function delete(string $id)
    {
        $foundMovie = $this->findById($id, true);

        if (!$foundMovie) {
            return false;
        }

        $foundMovie->genres()->detach();
        return $foundMovie->delete($id);
    }
}
