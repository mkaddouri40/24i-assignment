<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 24/07/2020
 * Time: 16:50
 */

namespace App\Src\Repositories\Movie;

use App\Src\Models\Movie\MovieModel;
use App\Src\Models\Search\SearchModel;

interface IMovieRepository
{

    /**
     * @param SearchModel $searchModel
     * @return mixed
     */
    public function get(SearchModel $searchModel);


    /**
     * @param MovieModel $movieModel
     * @return mixed
     */
    public function store(MovieModel $movieModel): MovieModel;

    /**
     * @param MovieModel $movieModel
     * @return mixed
     */
    public function update(MovieModel $movieModel);

    /**
     * @param $column
     * @param $value
     * @param bool $eloquentModel
     * @return mixed
     */
    public function findBy($column, $value, bool $eloquentModel = false);

    /**
     * @param string $id
     * @param bool $eloquentModel
     * @return mixed
     */
    public function findById(string $id, bool $eloquentModel = false);

    /**
     * @param string $id
     * @return mixed
     */
    public function delete(string $id);
}
