<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 28/07/2020
 * Time: 23:49
 */

namespace App\Src\Repositories\User;

use App\Src\Models\User\UserModel;

interface IUserRepository
{
    /**
     * @param UserModel $userModel
     * @return mixed
     */
    public function store(UserModel $userModel): UserModel;

    /**
     * @param $column
     * @param $value
     * @param bool $eloquentModel
     * @return mixed
     */
    public function findBy($column, $value, bool $eloquentModel = false);

    /**
     * @param string $email
     * @param bool $eloquentModel
     * @return mixed
     */
    public function findByEmail(string $email, bool $eloquentModel = false);

    /**
     * @param int $id
     * @param bool $eloquentModel
     * @return mixed
     */
    public function findById(int $id, bool $eloquentModel = false);
}
