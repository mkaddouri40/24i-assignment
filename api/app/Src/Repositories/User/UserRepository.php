<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 28/07/2020
 * Time: 23:49
 */

namespace App\Src\Repositories\User;

use App\Models\User;
use App\Src\Mappers\User\UserEloquentMapper;
use App\Src\Mappers\User\UserModelMapper;
use App\Src\Models\User\UserModel;

class UserRepository implements IUserRepository
{

    /**
     * @param UserModel $userModel
     * @return mixed
     */
    public function store(UserModel $userModel): UserModel
    {
        $user = UserModelMapper::toEloquentModel($userModel);
        $user->save();
        return UserEloquentMapper::toModel($user);
    }

    /**
     * @param $column
     * @param $value
     * @param bool $eloquentModel
     * @return UserModel|mixed|null
     */
    public function findBy($column, $value, bool $eloquentModel = false)
    {
        $user = User::where([$column => $value])->first();

        if ($eloquentModel) {
            return $user;
        }

        if (!$user) {
            return null;
        }

        return UserEloquentMapper::toModel($user);
    }

    /**
     * @param string $email
     * @param bool $eloquentModel
     * @return UserModel|mixed|null
     */
    public function findByEmail(string $email, bool $eloquentModel = false)
    {
        return $this->findBy('email', $email, $eloquentModel);
    }

    /**
     * @param int $id
     * @param bool $eloquentModel
     * @return UserModel|mixed|null
     */
    public function findById(int $id, bool $eloquentModel = false)
    {
        return $this->findBy('id', $id, $eloquentModel);
    }
}
