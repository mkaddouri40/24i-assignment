<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 24/07/2020
 * Time: 21:30
 */

namespace App\Src\Models\Search;

class PaginationModel
{
    /**
     * @var int
     */
    private $itemsPerPage;

    /**
     * @var int
     */
    private $totalItems;

    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $limit;

    /**
     * @return int
     */
    public function getItemsPerPage(): ?int
    {
        return $this->itemsPerPage;
    }

    /**
     * @param int $itemsPerPage
     * @return PaginationModel
     */
    public function setItemsPerPage(?int $itemsPerPage): PaginationModel
    {
        $this->itemsPerPage = $itemsPerPage;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalItems(): ?int
    {
        return $this->totalItems;
    }

    /**
     * @param int $totalItems
     * @return PaginationModel
     */
    public function setTotalItems(?int $totalItems): PaginationModel
    {
        $this->totalItems = $totalItems;
        return $this;
    }

    /**
     * @return int
     */
    public function getPage(): ?int
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return PaginationModel
     */
    public function setPage(?int $page): PaginationModel
    {
        if (!$page || $page < 1) {
            $page = 1;
        }
        $this->page = $page;
        return $this;
    }

    /**
     * @return int
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     * @return PaginationModel
     */
    public function setLimit(?int $limit): PaginationModel
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalPages(): ?int
    {
        if (!$this->getLimit()) {
            return 0;
        }

        return ceil($this->getTotalItems() / $this->getLimit());
    }
}
