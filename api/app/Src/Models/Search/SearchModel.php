<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 24/07/2020
 * Time: 21:30
 */

namespace App\Src\Models\Search;

use App\Src\Models\Genre\GenreModel;
use App\Src\Models\Movie\MovieModel;
use Illuminate\Support\Collection;

class SearchModel extends PaginationModel
{
    /**
     * @var string
     */
    private $orderBy;

    /**
     * @var string
     */
    private $direction;

    /**
     * @var MovieModel
     */
    private $movie;

    /**
     * @var GenreModel
     */
    private $genre;

    /**
     * @var Collection
     */
    private $items;

    /**
     * @return string
     */
    public function getOrderBy(): ?string
    {
        return $this->orderBy;
    }

    /**
     * @param string $orderBy
     * @return SearchModel
     */
    public function setOrderBy(?string $orderBy): SearchModel
    {
        $this->orderBy = $orderBy;
        return $this;
    }

    /**
     * @return string
     */
    public function getDirection(): ?string
    {
        return $this->direction;
    }

    /**
     * @param string $direction
     * @return SearchModel
     */
    public function setDirection(?string $direction): SearchModel
    {
        if (!$direction || strtolower($direction) !== 'asc' && strtolower($direction) !== 'desc') {
            $direction = 'asc';
        }
        $this->direction = $direction;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getItems(): ?Collection
    {
        return $this->items;
    }

    /**
     * @param Collection $items
     * @return SearchModel
     */
    public function setItems(?Collection $items): SearchModel
    {
        $this->items = $items;
        return $this;
    }

    /**
     * @return MovieModel
     */
    public function getMovie(): ?MovieModel
    {
        return $this->movie;
    }

    /**
     * @param MovieModel $movie
     * @return SearchModel
     */
    public function setMovie(?MovieModel $movie): SearchModel
    {
        $this->movie = $movie;
        return $this;
    }

    /**
     * @return GenreModel
     */
    public function getGenre(): ?GenreModel
    {
        return $this->genre;
    }

    /**
     * @param GenreModel $genre
     * @return SearchModel
     */
    public function setGenre(?GenreModel $genre): SearchModel
    {
        $this->genre = $genre;
        return $this;
    }
}
