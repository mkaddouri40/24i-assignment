<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 28/07/2020
 * Time: 23:19
 */

namespace App\Src\Models\User;

class UserModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var Carbon
     */
    private $email_verified_at;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $remember_token;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return UserModel
     */
    public function setId(?int $id): UserModel
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return UserModel
     */
    public function setName(?string $name): UserModel
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return UserModel
     */
    public function setEmail(?string $email): UserModel
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getEmailVerifiedAt(): ?Carbon
    {
        return $this->email_verified_at;
    }

    /**
     * @param Carbon $email_verified_at
     * @return UserModel
     */
    public function setEmailVerifiedAt(?Carbon $email_verified_at): UserModel
    {
        $this->email_verified_at = $email_verified_at;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return UserModel
     */
    public function setPassword(?string $password): UserModel
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getRememberToken(): ?string
    {
        return $this->remember_token;
    }

    /**
     * @param string $remember_token
     * @return UserModel
     */
    public function setRememberToken(?string $remember_token): UserModel
    {
        $this->remember_token = $remember_token;
        return $this;
    }
}
