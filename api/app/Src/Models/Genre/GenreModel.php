<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 26/07/2020
 * Time: 19:53
 */

namespace App\Src\Models\Genre;

use Carbon\Carbon;

class GenreModel
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Carbon
     */
    private $created_at;

    /**
     * @var Carbon
     */
    private $updated_at;

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return GenreModel
     */
    public function setId(?string $id): GenreModel
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return GenreModel
     */
    public function setName(?string $name): GenreModel
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->created_at;
    }

    /**
     * @param Carbon $created_at
     * @return GenreModel
     */
    public function setCreatedAt(?Carbon $created_at): GenreModel
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updated_at;
    }

    /**
     * @param Carbon $updated_at
     * @return GenreModel
     */
    public function setUpdatedAt(?Carbon $updated_at): GenreModel
    {
        $this->updated_at = $updated_at;
        return $this;
    }
}
