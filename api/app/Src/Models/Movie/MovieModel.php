<?php
/**
 * Created by PhpStorm.
 * User: mohamedkaddouri
 * Date: 24/07/2020
 * Time: 16:18
 */

namespace App\Src\Models\Movie;

use App\Src\Models\Genre\GenreModel;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class MovieModel
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Carbon
     */
    private $created_at;

    /**
     * @var Carbon
     */
    private $updated_at;

    /**
     * @var GenreModel
     */
    private $genre;

    /**
     * @var array
     */
    private $genre_ids;

    /**
     * @var Collection
     */
    private $genres;

    /**
     * @var array
     */
    private $genre_ids_to_remove;

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return MovieModel
     */
    public function setId(?string $id): MovieModel
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return MovieModel
     */
    public function setName(?string $name): MovieModel
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->created_at;
    }

    /**
     * @param Carbon $created_at
     * @return MovieModel
     */
    public function setCreatedAt(?Carbon $created_at): MovieModel
    {
        $this->created_at = $created_at;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updated_at;
    }

    /**
     * @param Carbon $updated_at
     * @return MovieModel
     */
    public function setUpdatedAt(?Carbon $updated_at): MovieModel
    {
        $this->updated_at = $updated_at;
        return $this;
    }

    /**
     * @return GenreModel
     */
    public function getGenre(): ?GenreModel
    {
        return $this->genre;
    }

    /**
     * @param GenreModel $genre
     * @return MovieModel
     */
    public function setGenre(?GenreModel $genre): MovieModel
    {
        $this->genre = $genre;
        return $this;
    }

    /**
     * @return array
     */
    public function getGenreIds(): ?array
    {
        return $this->genre_ids;
    }

    /**
     * @param array $genre_ids
     * @return MovieModel
     */
    public function setGenreIds(?array $genre_ids): MovieModel
    {
        $this->genre_ids = $genre_ids;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getGenres(): ?Collection
    {
        return $this->genres;
    }

    /**
     * @param Collection $genres
     * @return MovieModel
     */
    public function setGenres(?Collection $genres): MovieModel
    {
        $this->genres = $genres;
        return $this;
    }

    /**
     * @return array
     */
    public function getGenreIdsToRemove(): ?array
    {
        return $this->genre_ids_to_remove;
    }

    /**
     * @param array $genre_ids_to_remove
     * @return MovieModel
     */
    public function setGenreIdsToRemove(?array $genre_ids_to_remove): MovieModel
    {
        $this->genre_ids_to_remove = $genre_ids_to_remove;
        return $this;
    }
}
