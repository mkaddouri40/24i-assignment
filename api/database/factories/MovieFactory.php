<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use App\Models\Movie;
use Faker\Generator as Faker;

$factory->define(Movie::class, function (Faker $faker) {

    return [
        'id' => \Illuminate\Support\Str::uuid(),
        'name' => $faker->name
    ];
});
